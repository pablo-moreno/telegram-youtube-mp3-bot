FROM python:3.7.4

WORKDIR /app
ADD telegram_youtube_mp3_bot.py /app
ADD requirements.txt /app

RUN apt-get update
RUN apt-get install -y ffmpeg
RUN pip install --upgrade -r requirements.txt

ENTRYPOINT [ "python", "telegram_youtube_mp3_bot.py" ]
