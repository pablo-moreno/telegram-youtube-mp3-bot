# Telegram Youtube to MP3 Bot

Telegram Bot ready to download Youtube videos as MP3 with the greatest sound quality possible.

```sh
docker run -d -e TELEGRAM_API_TOKEN=<your-bot-api-token> registry.gitlab.com/pablo-moreno/telegram-youtube-mp3-bot 
```
