import os
import sys
import logging

from uuid import uuid4
from datetime import datetime

from telegram import InlineQueryResultArticle, ParseMode
from telegram.ext import Updater as Telegram, CommandHandler
from telegram.utils.helpers import escape_markdown

from youtube_dl import YoutubeDL as YouTube
from zipfile import ZipFile


API_TOKEN = os.environ.get('TELEGRAM_API_TOKEN')


def download_youtube(url, path, zipfile=False, keep=False):
    if not url:
        raise Exception('You have not specified a valid url')

    current_files = os.listdir(path)
    options = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '320',
        }],
        'outtmpl': os.path.join(path, '%(title)s.%(ext)s')
    }

    with YouTube(options) as youtube:
        youtube.download([url])

    diff_files = list(set(os.listdir(path)) - set(current_files))
    downloaded_files = list(map(lambda f: os.path.join(path, f), diff_files))

    return downloaded_files


def download(update, context):
    if os.environ.get('DOWNLOADING', 'FALSE') == 'TRUE':
        update.message.reply_text(f'Looks like someone is currently downloading something, please retry in a few minutes. ')
        return
    
    os.environ['DOWNLOADING'] = 'TRUE'
    message_url = update.message.text.replace('/download', '').strip()
    url = message_url.replace('https://youtu.be/', 'https://youtube.com/watch?v=')
    
    what_im_downloading = 'playlist' if 'playlist' in url else 'video'

    update.message.reply_text(f'I\'m downloading your {what_im_downloading}, this MAY take a while :)')
    files = download_youtube(url, path='.', zipfile=True)

    for file in files:
        update.message.reply_document(open(file, 'rb'))
        os.remove(file)

    os.environ['DOWNLOADING'] = 'FALSE'

def error(update, context):
    """Log Errors caused by Updates."""
    update.message.reply_text(f'Ups, something went wrong: {context.error}')


def main():
    tg = Telegram(API_TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = tg.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("download", download))

    dp.add_error_handler(error)

    # Start the Bot
    tg.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    tg.idle()


if __name__ == '__main__':
    main()
